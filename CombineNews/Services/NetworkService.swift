//
//  NetworkService.swift
//  CombineNews
//
//  Created by Oleg Babura on 12.01.2022.
//

import Foundation
import Combine

enum NetworkError: Error {
    case url
    case request(Error)
    case decode
}

protocol NetworkServiceProtocol {
    func search(_ query: String?) -> AnyPublisher<[Article], Error>
}

class NetworkService: NetworkServiceProtocol {
    func search(_ query: String?) -> AnyPublisher<[Article], Error> {
        return URLSession.shared.dataTaskPublisher(for: searchUrl(query)!)
            .map({ $0.data })
            .decode(type: ArticleResponce.self, decoder: JSONDecoder())
            .mapError({ error in return NetworkError.request(error) })
            .map({ $0.articles })
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    //TODO: implement url builder
    func searchUrl(_ query: String?) -> URL? {
        let apiKey: String = "0dc7040ec6f04d1790764a43c704fd9b"
        guard let query = query else {
            return nil
        }

        return URL(string: "https://newsapi.org/v2/everything?q=\(query)&sortBy=popularity&apiKey=\(apiKey)")
    }
}
