//
//  FeedCellViewModel.swift
//  CombineNews
//
//  Created by Oleg Babura on 13.01.2022.
//

import Foundation

class FeedCellViewModel {
    @Published var title: String!
    @Published var description: String!
    @Published var date: Date!
    
    private var article: Article
    
    init(article: Article) {
        self.article = article
        setupBindings()
    }
    
    func setupBindings() {
        title = article.title
        description = article.description
        date = article.date
    }
}
