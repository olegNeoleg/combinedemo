//
//  FeedCell.swift
//  CombineNews
//
//  Created by Oleg Babura on 13.01.2022.
//

import UIKit

class FeedCell: UITableViewCell {
    static let identifier = "FeedCell"
    
    lazy var labelsStack = UIStackView()
    lazy var titleLabel = UILabel()
    lazy var descriptionLabel = UILabel()
    lazy var dateLabel = UILabel()
    
    var viewModel: FeedCellViewModel! {
        didSet { setupViewModel() }
    }
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        return formatter
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubviews()
        setupConstraints()
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        contentView.addSubview(labelsStack)
        labelsStack.translatesAutoresizingMaskIntoConstraints = false
        
        [titleLabel, descriptionLabel, dateLabel].forEach({
            labelsStack.addArrangedSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        })
    }
    
    func setupConstraints() {
        let defaultVerticalMargin: CGFloat = 16.0
        let defaultHorizontalMargin: CGFloat = 16.0
        
        NSLayoutConstraint.activate([
            labelsStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: defaultVerticalMargin),
            labelsStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: defaultHorizontalMargin),
            labelsStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -defaultHorizontalMargin),
            labelsStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -defaultVerticalMargin)
        ])
    }
    
    func setupViews() {
        labelsStack.distribution = .fill
        labelsStack.axis = .vertical
        labelsStack.spacing = 8
        
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        titleLabel.numberOfLines = 0
        
        descriptionLabel.numberOfLines = 0
        
        dateLabel.font = UIFont.systemFont(ofSize: 14)
        dateLabel.textColor = .gray
    }
    
    func setupViewModel() {
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        dateLabel.text = dateFormatter.string(from: viewModel.date)
    }
}
