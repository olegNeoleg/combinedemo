//
//  FeedViewModel.swift
//  CombineNews
//
//  Created by Oleg Babura on 12.01.2022.
//

import Foundation
import Combine

enum FeedViewModelError: Error, Equatable {
    case articlesFetch
}

enum FeedViewModelState: Equatable {
    case initial
    case loading
    case loaded
    case error(FeedViewModelError)
}

class FeedViewModel {
    enum Section { case articles }
    
    @Published private(set) var articles: [Article] = []
    @Published private(set) var state: FeedViewModelState = .initial
    
    private let networkService: NetworkServiceProtocol
    private var bindings = Set<AnyCancellable>()
    
    init(networkService: NetworkServiceProtocol = NetworkService()) {
        self.networkService = networkService
    }
    
    func search(query: String) {
        if query == "" {
            state = .initial
            return
        }
        
        state = .loading
        
        let completionHandler: (Subscribers.Completion<Error>) -> Void = { [weak self] completion in
            switch completion {
            case .finished:
                self?.state = .loaded
            case .failure:
                self?.state = .error(.articlesFetch)
            }
        }
        
        let valueHandler: ([Article]) -> Void = { [weak self] articles in
            self?.articles = articles
        }
        
        networkService.search(query)
            .sink(receiveCompletion: completionHandler, receiveValue: valueHandler)
            .store(in: &bindings)
    }
}
