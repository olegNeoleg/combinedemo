//
//  FeedViewController.swift
//  CombineNews
//
//  Created by Oleg Babura on 12.01.2022.
//

import UIKit
import Combine

class FeedViewController: UIViewController {
    private typealias DataSource = UITableViewDiffableDataSource<FeedViewModel.Section, Article>
    private typealias Snapshot = NSDiffableDataSourceSnapshot<FeedViewModel.Section, Article>

    private lazy var contentView = FeedView()
    private let viewModel: FeedViewModel
    private var bindings = Set<AnyCancellable>()
    
    private var dataSource: DataSource!
    
    init(viewModel: FeedViewModel = FeedViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        contentView.tableView.register(FeedCell.self, forCellReuseIdentifier: FeedCell.identifier)
        
        configureDataSource()
        setupBindings()
    }
    
    //MARK: - Bindings
    
    func setupBindings() {
        func bindViewToViewModel() {
            
            contentView.searchTextField.textPublisher
                .debounce(for: 0.3, scheduler: RunLoop.main)
                .removeDuplicates()
                .sink { [weak viewModel] in
                    viewModel?.search(query: $0)
                }
                .store(in: &bindings)
        }
        
        func bindViewModelToView() {
            
            let stateHandler: (FeedViewModelState) -> Void = { [weak self] in
                switch $0 {
                case .initial:
                    debugPrint("initial")
                case .loading:
                    debugPrint("loading")
                case .loaded:
                    debugPrint("loaded")
                case .error(let error):
                    self?.showError(error)
                }
            }
            
            viewModel.$state
                .receive(on: RunLoop.main)
                .sink(receiveValue: stateHandler)
                .store(in: &bindings)
            
            viewModel.$articles
                .receive(on: RunLoop.main)
                .sink(receiveValue: { [weak self] _ in
                    self?.updateSections()
                })
                .store(in: &bindings)
        }
        
        bindViewToViewModel()
        bindViewModelToView()
    }
    
    private func showError(_ error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func updateSections() {
        var snapshot = Snapshot()
        snapshot.appendSections([.articles])
        snapshot.appendItems(viewModel.articles)
        dataSource.apply(snapshot, animatingDifferences: true)
    }
}

// MARK: - UITableViewDataSource

extension FeedViewController {
    func configureDataSource() {
        dataSource = DataSource(
            tableView: contentView.tableView,
            cellProvider: { tableView, indexPath, article in
                let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell.identifier) as? FeedCell
                
                cell?.viewModel = FeedCellViewModel(article: article)
                
                return cell
            })
    }
}

