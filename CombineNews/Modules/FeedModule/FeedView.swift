//
//  FeedView.swift
//  CombineNews
//
//  Created by Oleg Babura on 12.01.2022.
//

import UIKit

class FeedView: UIView {
    lazy var searchTextField = UITextField()
    lazy var tableView = UITableView()
    
    init() {
        super.init(frame: .zero)
        
        addSubviews()
        setupConstraints()
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubviews() {
        [searchTextField, tableView].forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    func setupConstraints() {
        let defaultHorizontalMargin: CGFloat = 16.0
        let defaultHeight: CGFloat = 30.0
        
        NSLayoutConstraint.activate([
            searchTextField.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            searchTextField.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: defaultHorizontalMargin),
            searchTextField.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -defaultHorizontalMargin),
            searchTextField.heightAnchor.constraint(equalToConstant: defaultHeight),
            
            tableView.topAnchor.constraint(equalTo: searchTextField.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    func setupViews() {
        searchTextField.borderStyle = .roundedRect
        searchTextField.autocorrectionType = .no
        
        tableView.separatorStyle = .none
    }
}
