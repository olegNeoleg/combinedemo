//
//  News.swift
//  CombineNews
//
//  Created by Oleg Babura on 12.01.2022.
//

import Foundation

fileprivate let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    return formatter
}()

struct Article: Codable, Identifiable, Hashable {
    let id = UUID()
    var title: String
    var description: String
    var date: Date
    
    enum CodingKeys: String, CodingKey {
        case title
        case description
        case date = "publishedAt"
    }
}

extension Article {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        description = try container.decode(String.self, forKey: .description)
        let dateString = try container.decode(String.self, forKey: .date)
        
        guard let formattedDate = dateFormatter.date(from: dateString) else {
            throw DecodingError.dataCorruptedError(forKey: .date,
                                                   in: container,
                                                   debugDescription: "Date string doesn't match expected format")
        }
        date = formattedDate
    }
}
