//
//  ArticleResponce.swift
//  CombineNews
//
//  Created by Oleg Babura on 24.01.2022.
//

import Foundation

struct ArticleResponce: Codable {
    var status: String
    var articles: [Article]
}
